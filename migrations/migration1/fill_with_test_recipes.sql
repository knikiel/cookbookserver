INSERT INTO recipes (name, description, source, animalness, carbiness, last_modified)
VALUES ('Spinat-Suppe', 'Mit und ohne Ei', 'Kochbuch1, S.13', 1, 1, '2015-03-02');

INSERT INTO recipes (name, source, last_modified)
VALUES ('Asiapfanne', 'Kochbuch1, S.14', NOW());

INSERT INTO recipes (name, source, animalness, carbiness, last_modified)
VALUES ('Gemüsepfanne', 'http://www.link.com', 2, 1, NOW());
