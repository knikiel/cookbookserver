CREATE TABLE ingredients
(
  ingredients_id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  recipes_id INT NOT NULL REFERENCES recipes (recipes_id)
);