CREATE TABLE recipes
(
  recipes_id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT,
  source TEXT NOT NULL,
  animalness INT DEFAULT 0 NOT NULL,
  carbiness INT DEFAULT 0 NOT NULL,
  last_modified TIMESTAMP WITH TIMEZONE NOT NULL
);