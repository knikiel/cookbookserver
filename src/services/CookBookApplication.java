package services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

// Management of web services.
@ApplicationPath("/")
public class CookBookApplication extends Application{
    // Returns the class files of the web services.
    @Override
    public Set<Class<?>> getClasses() {
        HashSet<Class<?>> classes = new HashSet<>();
        classes.add(RecipesResource.class);
        return classes;
    }
}
