package services;

import data.Database;
import data.DatabaseException;
import data.Ingredient;
import data.Recipe;

import javax.json.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.sql.Timestamp;
import java.util.List;
import java.util.logging.Logger;


// Offers RESTful CRUD-services involving recipes.
@Path("/recipes")
public class RecipesResource {
    private static Logger mLogger;
    static {
        mLogger = Logger.getLogger(RecipesResource.class.getName());
    }

    // RESTful service: Get all recipes (ID, name, last_modified).
    // Also returns the current server time for later client requests.
    // Query parameter "since" is used for filtering the recipes.
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllRecipes(@DefaultValue("") @QueryParam("since") Timestamp timestamp) {
        JsonObjectBuilder responseBuilder = Json.createObjectBuilder();
        try {
            responseBuilder.add("server_time", Database.getServerTime().toString());
            JsonArrayBuilder recipesBuilder = Json.createArrayBuilder();
            for (Recipe recipe : Database.readAllRecipesSince(timestamp)) {
                JsonObjectBuilder recipeBuilder = Json.createObjectBuilder();
                recipeBuilder.add("id", recipe.getId());
                recipeBuilder.add("name", recipe.getName());
                recipeBuilder.add("last_modified", recipe.getLastModified());
                recipesBuilder.add(recipeBuilder.build());
            }
            responseBuilder.add("recipes", recipesBuilder);
        }
        catch (DatabaseException e) {
            mLogger.severe(e.getInnerException().getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        JsonObject jsonRecipes = responseBuilder.build();
        return Response.ok(jsonRecipes.toString()).build();
    }

    // RESTful service: CREATE a new recipe.
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createRecipe(Recipe recipe) {
        if (!recipe.validate()) {
            // TODO: genauere Fehlerangabe
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        try {
            Database.createRecipe(recipe);
        }
        catch (DatabaseException e) {
            mLogger.severe(e.getInnerException().getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.created(URI.create("recipes/" + recipe.getId())).build();
    }

    // RESTful service: READ a recipe.
    @GET @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readRecipe(@PathParam("id") Integer id) {
        Recipe recipe;
        try {
            recipe = Database.readRecipe(id);
        }
        catch (DatabaseException e) {
            mLogger.severe(e.getInnerException().getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        if (recipe == null) {
            // no recipe to read
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(recipe).build();
    }

    // RESTful service: UPDATE a recipe.
    @PUT @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response replaceRecipe(@PathParam("id") Integer id, Recipe recipe) {
        if (!recipe.validate()) {
            // TODO: genauere Fehlerangabe
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        Recipe oldRecipe;
        try {
            oldRecipe = Database.readRecipe(id);
        }
        catch (DatabaseException e) {
            mLogger.severe(e.getInnerException().getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        if (oldRecipe == null) {
            // no recipe to update
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        recipe.setId(id);
        try {
            Database.replaceRecipe(id, recipe);
        }
        catch (DatabaseException e) {
            mLogger.severe(e.getInnerException().getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.created(URI.create("recipes/" + id)).build();
    }

    // RESTful service: DELETE a recipe.
    @DELETE @Path("{id}")
    public Response deleteRecipe(@PathParam("id") Integer id) {
        boolean isValid;
        try {
            isValid = Database.deleteRecipe(id);
        }
        catch (DatabaseException e) {
            mLogger.severe(e.getInnerException().getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        if (!isValid) {
            // no recipe to remove
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok().build();
    }

    // RESTful service: READ the ingredients of a recipe.
    @GET @Path("{id}/ingredients")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readIngredients(@PathParam("id") Integer id) {
        List<Ingredient> ingredients;
        JsonObjectBuilder responseBuilder = Json.createObjectBuilder();

        // read from database
        try {
            responseBuilder.add("server_time", Database.getServerTime().toString());
            ingredients = Database.readIngredients(id);
        }
        catch (DatabaseException e) {
            mLogger.severe(e.getInnerException().getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        // serialize
        JsonArrayBuilder ingredientsBuilder = Json.createArrayBuilder();
        for (Ingredient ingredient : ingredients) {
            ingredientsBuilder.add(ingredient.getName());
        }
        responseBuilder.add("ingredients", ingredientsBuilder.build());
        JsonObject jsonRecipes = responseBuilder.build();
        return Response.ok(jsonRecipes.toString()).build();
    }

    // RESTful service: UPDATE the ingredients of a recipe.
    @PUT @Path("{id}/ingredients")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateIngredients(@PathParam("id") Integer id, List<String> ingredients) {
        for (String ingredient : ingredients) {
            // ingredient names must be at least 2 chars long
            if (ingredient.length() < 2) {
                // TODO: genauere Fehlerangabe
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }
        try {
            Database.replaceIngredients(id, ingredients);
        }
        catch (DatabaseException e) {
            mLogger.severe(e.getInnerException().getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return Response.created(URI.create("recipes/" + id + "/ingredients")).build();
    }
}
