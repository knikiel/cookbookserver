package data;

// Generic exception class for database problems.
// Basically a wrappe.
public class DatabaseException extends Exception {
    private Exception mInner;

    // TODO: schöner durch Basisklassen-Konstruktor: (message, cause)
    public DatabaseException(Exception e) {
        mInner = e;
    }

    public Exception getInnerException() {
        return mInner;
    }
}
