package data;

import javax.xml.bind.annotation.XmlRootElement;

// TODO: überlege, ob Trennung von Eingaberezept und Datenbankrezept sinnvoll ist
// Contains the important attributes of a recipe, except for ingredients and tags.
@XmlRootElement
public class Recipe {
    public enum Animalness {
        MEATY,
        VEGETARIAN,
        VEGAN
    }

    public enum Carbiness {
        HIGH,
        LOW
    }

    private Integer mId;

    private String mName;
    private String mDescription;
    private String mSource;
    private Animalness mAnimalness;
    private Carbiness mCarbiness;

    private String mLastModified;

    public Recipe() { }

    public  Recipe(int id, String name, String description, String source, Animalness animalness, Carbiness carbiness, String lastModified) {
        mId = id;
        mName = name;
        mDescription = description;
        mSource = source;
        mAnimalness = animalness;
        mCarbiness = carbiness;
        mLastModified = lastModified;
    }

    public Integer getId() {return mId;}

    public void setId(Integer id) {mId = id;}

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name.trim();
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description.trim();
    }

    public void setSource(String source) { mSource = source.trim(); }

    public String getSource() { return mSource; }

    public void setAnimalness(Animalness animalness) { mAnimalness = animalness; }

    public Animalness getAnimalness() { return mAnimalness; }

    public void setCarbiness(Carbiness carbiness) { mCarbiness = carbiness; }

    public Carbiness getCarbiness() { return mCarbiness; }

    public void setLastModified(String lastModified) {
        mLastModified = lastModified.trim();
    }

    public String getLastModified() { return mLastModified; }

    // Validate recipe from client.
    // Id must not be set.
    // Name must be set and at least 3 chars long.
    // Source must be set and at least 3 chars long.
    // LastModified must not be set.
    public boolean validate() {
        if (mId != null) {
            return false;
        }

        if (mName == null || mName.length() < 3) {
            return false;
        }

        if (mSource == null || mSource.length() < 3) {
            return false;
        }

        if (mLastModified != null) {
            return false;
        }

        return true;
    }

}
