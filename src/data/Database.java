package data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

// TODO: denke über Connectionpools nach
// TODO: denke über effizientere Lösung als GET_SERVER_TIME_SQL nach
// TODO: Java-Objekte/Methoden verwenden evtl. nicht konsistente Timestamps (ohne Zeitzone)
// Access to the database.
public class Database {
    static final private String DATABASE_URL;
    static final private String DATABASE_USERNAME;
    static final private String DATABASE_PASSWORD;
    static private Connection mConnection = null;
    static {
        // read database connection information from environment variables
        DATABASE_URL = System.getenv("DATABASE_URL");
        DATABASE_USERNAME = System.getenv("DATABASE_USERNAME");
        DATABASE_PASSWORD = System.getenv("DATABASE_PASSWORD");
    }

    // SQL code of different database interactions
    static final private String GET_SERVER_TIME_SQL = "SELECT NOW() AS server_time;";

    static final private String READ_ALL_RECIPES_SQL = "SELECT * FROM recipes;";
    static final private String READ_ALL_RECIPES_SINCE_SQL
            = "SELECT * FROM recipes "
                + "WHERE last_modified > ?;";
    static final private String READ_RECIPE_SQL = "SELECT * FROM recipes WHERE recipes_id = ?;";
    static final private  String CREATE_RECIPE_SQL
            = "INSERT INTO recipes (name, description, source, animalness, carbiness, last_modified) "
                + "VALUES (?, ?, ?, ?, ?, NOW());";
    static final private String REPLACE_RECIPE_SQL
            = "UPDATE recipes "
                + "SET name = ?, description = ?, source = ?, "
                + "animalness = ?, carbiness = ?, last_modified = NOW() "
                + "WHERE recipes_id = ?;";
    static final private String DELETE_RECIPE_SQL = "DELETE FROM recipes WHERE recipes_id = ?;";

    private static final String READ_INGREDIENTS_SQL
            = "SELECT ingredients_id, name FROM ingredients WHERE recipes_id = ?;";
    private static final String DELETE_INGREDIENTS_SQL
            = "DELETE FROM ingredients WHERE recipes_id = ?; ";
    private static final String CREATE_INGREDIENT_SQL
            = "INSERT INTO ingredients (recipes_id, name) "
                + "VALUES (?, ?);";
    private static final String UPDATE_LAST_MODIFIED_SQL
            = "UPDATE recipes SET last_modified = NOW() WHERE recipes_id = ?; ";

    // Connect to external database.
    static public void connect()
        throws DatabaseException
    {
        try {
            if (mConnection != null && !mConnection.isClosed()) {
                // already connected
                return;
            }
            Class.forName("org.postgresql.Driver");
            mConnection = DriverManager.getConnection(
                DATABASE_URL, DATABASE_USERNAME, DATABASE_PASSWORD);
        }
        catch (SQLException | ClassNotFoundException e) {
            mConnection = null;
            throw new DatabaseException(e);
        }
    }

    // Returns whether connected to database.
    static public boolean isConnected()
        throws DatabaseException
    {
        try {
            return mConnection != null && !mConnection.isClosed();
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    // Disconnect from external database.
    static private void disconnect()
        throws DatabaseException
    {
        try {
            if (mConnection == null || mConnection.isClosed()) {
                // already disconnected
                mConnection = null;
                return;
            }
            mConnection.close();
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    // Returns the current time of (database) server.
    static public Timestamp getServerTime()
        throws DatabaseException
    {
        connect();
        try(
            Statement statement = mConnection.createStatement();
            ResultSet databaseResults = statement.executeQuery(GET_SERVER_TIME_SQL)
        ) {
            databaseResults.next();
            return databaseResults.getTimestamp("server_time");
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    // Get all recipes, changed or added after timestamp.
    // If timestamp is null, get all recipes.
    static public List<Recipe> readAllRecipesSince(Timestamp timestamp)
        throws DatabaseException
    {
        connect();
        List<Recipe> recipes;
        if (timestamp == null) {
            // no timestamp given
            try (
                Statement statement = mConnection.createStatement();
                ResultSet databaseResults = statement.executeQuery(READ_ALL_RECIPES_SQL)
            ) {
                // collect query results
                recipes = new ArrayList<>();
                while (databaseResults.next()) {
                    recipes.add(constructRecipe(databaseResults));
                }
            } catch (SQLException e) {
                throw new DatabaseException(e);
            }
        } else {
            // timestamp given
            try (
                PreparedStatement statement = mConnection.prepareStatement(READ_ALL_RECIPES_SINCE_SQL)
            ) {
                statement.setTimestamp(1, timestamp);
                try (
                    ResultSet databaseResults = statement.executeQuery()
                ) {
                    // collect query results
                    recipes = new ArrayList<>();
                    while (databaseResults.next()) {
                        recipes.add(constructRecipe((databaseResults)));
                    }
                }
            } catch (SQLException e) {
                throw new DatabaseException(e);
            }
        }

        return recipes;
    }

    // Create the recipe.
    // Modify ID in recipe.
    static public void createRecipe(Recipe recipe)
        throws DatabaseException
    {
        // assert: recipe is valid
        connect();
        try (
            PreparedStatement preparedStatement = mConnection.prepareStatement(CREATE_RECIPE_SQL)
        ) {
            fillPreparedStatement(preparedStatement, recipe);
            boolean isExecuted = preparedStatement.execute();
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    // Get the recipe.
    // Returns null if not found.
    static public Recipe readRecipe(int id)
        throws DatabaseException
    {
        connect();

        Recipe recipe = null;
        try (
            PreparedStatement preparedStatement = mConnection.prepareStatement(READ_RECIPE_SQL)
        ) {
            preparedStatement.setInt(1, id);
            try (
                    ResultSet databaseResults = preparedStatement.executeQuery()
            ) {
                // collect query result
                while (databaseResults.next()) {
                    recipe = constructRecipe(databaseResults);
                }
            }
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }

        return recipe;
    }

    // Replace the recipe.
    static public void replaceRecipe(int id, Recipe recipe)
        throws DatabaseException
    {
        // assert: recipe is valid
        connect();

        try (
            PreparedStatement preparedStatement = mConnection.prepareStatement(REPLACE_RECIPE_SQL)
        ) {
            fillPreparedStatement(preparedStatement, recipe);
            preparedStatement.setInt(6, id);
            boolean isExecuted = preparedStatement.execute();
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }
    }

    // Deletes the recipe.
    // Returns whether found.
    static public boolean deleteRecipe(int id)
        throws DatabaseException
    {
        connect();

        try (
            PreparedStatement preparedStatement = mConnection.prepareStatement(DELETE_RECIPE_SQL)
        ) {
            preparedStatement.setInt(1, id);
            boolean isExecuted = preparedStatement.execute();
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }

        return true;
    }

    // Reads the ingredients of a recipe.
    static public List<Ingredient> readIngredients(int id)
        throws DatabaseException
    {
        connect();

        List<Ingredient> ingredients;
        try (
            PreparedStatement preparedStatement = mConnection.prepareStatement(READ_INGREDIENTS_SQL)
        ) {
            preparedStatement.setInt(1, id);
            try (
                ResultSet databaseResults = preparedStatement.executeQuery()
            ) {
                ingredients = new ArrayList<>();
                while (databaseResults.next()) {
                    ingredients.add(constructIngredient(databaseResults));
                }
            }
        }
        catch (SQLException e) {
            throw new DatabaseException(e);
        }

        return ingredients;
    }

    // Replace the ingredients of a recipe.
    // Removes the previous ingredients of recipe with ID id, then inserts new ones.
    // Also changes "last_modified" column of recipe.
    static public void replaceIngredients(int id, List<String> ingredients)
        throws DatabaseException
    {
        connect();

        try {
            // begin transaction
            mConnection.setAutoCommit(false);
            try (
                PreparedStatement deleteStatement = mConnection.prepareStatement(DELETE_INGREDIENTS_SQL);
                PreparedStatement timestampStatement = mConnection.prepareStatement(UPDATE_LAST_MODIFIED_SQL)
            ) {
                // delete old ingredients
                deleteStatement.setInt(1, id);
                deleteStatement.execute();

                // update timestamp in recipe
                timestampStatement.setInt(1, id);
                timestampStatement.execute();

                // add new ingredients one after another
                try (
                    PreparedStatement insertStatement = mConnection.prepareStatement(CREATE_INGREDIENT_SQL)
                ) {
                    for (String ingredient : ingredients) {
                        insertStatement.setInt(1, id);
                        insertStatement.setString(2, ingredient);
                        insertStatement.execute();
                    }
                }

            }
            mConnection.commit();
            mConnection.setAutoCommit(true);
        }
        catch (SQLException e) {
            try {
                mConnection.rollback();
                mConnection.setAutoCommit(true);
            }
            catch (SQLException e_inner) {
                throw  new DatabaseException(e_inner);
            }
            throw new DatabaseException(e);
        }
    }

    // Create recipe from the resultSet.
    static private Recipe constructRecipe(ResultSet resultSet)
        throws SQLException
    {
        Recipe recipe = new Recipe(
                resultSet.getInt("recipes_id"),
                resultSet.getString("name"),
                resultSet.getString("description"),
                resultSet.getString("source"),
                Recipe.Animalness.values()[resultSet.getInt("animalness")],
                Recipe.Carbiness.values()[resultSet.getInt("carbiness")],
                resultSet.getTimestamp("last_modified").toString() );
        return recipe;
    }

    // Create ingredient from the resultSet.
    static private Ingredient constructIngredient(ResultSet resultSet)
            throws SQLException
    {
        Ingredient ingredient = new Ingredient(
                resultSet.getInt("ingredients_id"),
                resultSet.getString("name"));
        return ingredient;
    }

    // Set the fields in the prepared statement according to the given recipe.
    // Ignores ID and LastModified.
    static private void fillPreparedStatement(PreparedStatement statement, Recipe recipe)
        throws SQLException
    {
        statement.setString(1, recipe.getName());
        statement.setString(2, recipe.getDescription());
        statement.setString(3, recipe.getSource());
        Recipe.Animalness animalness = recipe.getAnimalness();
        if (animalness != null) {
            statement.setInt(4, animalness.ordinal());
        } else {
            statement.setInt(4,0);
        }
        Recipe.Carbiness carbiness = recipe.getCarbiness();
        if (carbiness != null) {
            statement.setInt(5, carbiness.ordinal());
        } else {
            statement.setInt(5, 0);
        }
    }
}
