package data;

public class Ingredient {
    private Integer mId;

    private String mName;

    public Ingredient(Integer id, String name) {
        mId = id;
        mName = name;
    }

    public Integer getId() { return mId; }

    public void setId(Integer id) { mId = id; }

    public String getName() { return mName; }

    public void setName(String name) { mName = name.toUpperCase(); }

    // Validate ingredient from client.
    // Id must not be set.
    // Name must be set, at least 2 chars long.
    public boolean validate() {
        if (mId != null) {
            return false;
        }

        if (mName == null || mName.length() < 2) {
            return false;
        }

        return true;
    }
}
